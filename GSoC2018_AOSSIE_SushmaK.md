<img src="https://i.imgur.com/MXb1jv6.png" align="center" width="420" height="100"/>

# GSoC 2018 Work Product - Sushma Kukkadapu

**Student** : [Sushma Kukkadapu](https://gitlab.com/KukkadapuSushma)

**Organisation** : [AOSSIE](http://aossie.org/)

**Project** : MindTheWord   &   MindTheWord-Desktop

# About Project

**MindTheWord** is currently browser extension that helps users to infer meaning of a new language they wish to learn by translating certain percentage of words in a page.

In every webpage visited, it randomly translates a few words into the target language specified by the user. Because the translated words are kept in context, it is easy to infer and memorize their meanings. Therefore, users can effortlessly learn new vocabulary and improve the language skills.

In the options, one can configure:
* how much should be translated;
* how translated words or characters should be highlighted;
* a blacklist of websites that should not be translated;
* and much more.....

# Project repository

[AOSSIE : MindTheWord](https://gitlab.com/aossie/MindTheWord)

[AOSSIE : MindTheWord-Desktop](https://gitlab.com/aossie/MindTheWord-Desktop)


# Summary 

I proposed to enhance MindTheWord Browser Extension User-Interface which adds it a polished look to sustain in the competition from other such extensions. I have also proposed to develop MindTheWord's Desktop Application which can work on local document files and improves its availability to wider audience by being MTW Browser Extension's desktop version.

I worked on the MindTheWord Browser Extension and Desktop application during my summers with AOSSIE, in which my work involved enhacement of user-interface of Browser extension and development of Desktop version of it. My work on Browser extension made it user-friendly and chrome web store ready. Also, addition of new features for MindTheWord Browser extension like tooltips, navigation menu bars are undertaken. I have also finished the front-end development of Desktop app version of MindTheWord from scratch.


# Merge Requests

## My Merged requests for MindTheWord Browser Extension

* [MR 258: Improved UI of Quiz page][https://gitlab.com/aossie/MindTheWord/merge_requests/258](https://gitlab.com/aossie/MindTheWord/merge_requests/258)

* [MR 266: Removal of unsupported languages in Description.md][https://gitlab.com/aossie/MindTheWord/merge_requests/266](https://gitlab.com/aossie/MindTheWord/merge_requests/266)

* [MR 267: Removed unused files/directories][https://gitlab.com/aossie/MindTheWord/merge_requests/267](https://gitlab.com/aossie/MindTheWord/merge_requests/267)

* [MR 268: Fixes UI bug-addition of responsiveness to navigation menu bar][https://gitlab.com/aossie/MindTheWord/merge_requests/268](https://gitlab.com/aossie/MindTheWord/merge_requests/268)

* [MR 269: Added new language descriptions][https://gitlab.com/aossie/MindTheWord/merge_requests/269](https://gitlab.com/aossie/MindTheWord/merge_requests/269)

* [MR 270: Added hovered descriptions][https://gitlab.com/aossie/MindTheWord/merge_requests/270](https://gitlab.com/aossie/MindTheWord/merge_requests/270)

* [MR 272: Updation of screenshots with new UI][https://gitlab.com/aossie/MindTheWord/merge_requests/272](https://gitlab.com/aossie/MindTheWord/merge_requests/272)

* [MR 276: Adjusted padding and font sizes][https://gitlab.com/aossie/MindTheWord/merge_requests/276](https://gitlab.com/aossie/MindTheWord/merge_requests/276)

[Summary of all my **ISSUES** related to MindTheWord Browser Extension](https://gitlab.com/aossie/MindTheWord/issues?scope=all&utf8=%E2%9C%93&state=all&assignee_username[]=KukkadapuSushma)


## My Merged requests for MindTheWord-Desktop

* [MR1: Configuration of package.json][https://gitlab.com/aossie/MindTheWord-Desktop/merge_requests/1](https://gitlab.com/aossie/MindTheWord-Desktop/merge_requests/1)

### Open

* [MR4: Added front-end to upload docs by user][https://gitlab.com/aossie/MindTheWord-Desktop/merge_requests/4](https://gitlab.com/aossie/MindTheWord-Desktop/merge_requests/4)

* [MR5: Developed UI to preview uploaded file][https://gitlab.com/aossie/MindTheWord-Desktop/merge_requests/5](https://gitlab.com/aossie/MindTheWord-Desktop/merge_requests/5)

* [MR6: Updated README][https://gitlab.com/aossie/MindTheWord-Desktop/merge_requests/6](https://gitlab.com/aossie/MindTheWord-Desktop/merge_requests/6)

* [MR7: Added options to save and download documents transalted][https://gitlab.com/aossie/MindTheWord-Desktop/merge_requests/7](https://gitlab.com/aossie/MindTheWord-Desktop/merge_requests/7)

* [MR8: Added pdf file previewing option][https://gitlab.com/aossie/MindTheWord-Desktop/merge_requests/8](https://gitlab.com/aossie/MindTheWord-Desktop/merge_requests/8)

[Summary of all my **ISSUES** related to MindTheWord Desktop app](https://gitlab.com/aossie/MindTheWord-Desktop/issues?scope=all&utf8=%E2%9C%93&state=all&author_username=KukkadapuSushma)


# Enhancement of MindTheWord Browser Extension

I have enhanced the user-interface looks of the MindTheWord browser extension during my first and second phases. As users feel more comfortable if the contents are displayed in the hierarchical fashion, so I added navigation menus which gave the Extension a clean look. These menus are responsible for dividing the options into menu-lists that adds user-friendliness. I used Bootstrap technology to make the web extension responsive in order to fit on all kinds of screens. I have also implemented tool-tips on hovering the menu items, that is when the user hovers onto any one of the menu item options then it's description is displayed which increases user understanding of what that option is responsible for. Thus, I have improved the readability of the extension.

Also, I enhanced the UI of the Quiz Page. I added the 'panels frame' to fit the quiz questions into the body area and made the overall look of the Quiz Page responsive. I have also translated the instructions of using the Web Extension from English to Thai, Irish and Belarusian for a global outreach. I have updated the new themed look of browser extension's screenshots which are helpful for the chrome extension store users.

The enhancement of the design involved creative thinking to make the overall feel of UI user-friendly and production ready. I had done manual testing in users point of view upon all MRs got merged. During testing, I fixed minor bugs and issues in order to make the Browser Extension robust.

The technologies I used in the development of the MindTheWord Browser Extension are Bootstrap, JavaScript, HTML, CSS and AngularJS. Also, I have written blogs about MindTheWord-AOSSIE and increased the activity of AOSSIE's Twitter Channel by encouraging my peers to tweet about their developments.

## Screenshots of MindTheWord - Browser Extension

<table>
  <tr>
    <td><img src="https://i.imgur.com/DA32t5X.png" /></td>
    <td><img src="https://i.imgur.com/vYyJWZd.png"/></td>
  </tr>
  <tr>
    <td><img src="https://i.imgur.com/uYbHiCo.png" /></td>
    <td><img src="https://i.imgur.com/2ZcPbe8.png" /></td>
  </tr>
</table>


# Development of MindTheWord Desktop Application

The idea of making MindTheWord more reachable, my mentor suggested to bring up the development of Desktop app version of the Browser Extension. I have chosen ElectronJS as it supports cross-platform desktop apps development. I designed the front-end of the Desktop App first. The front end included the user's choice of the document to be uploaded that the user wants to translate. So, I have developed the user-interface with the Upload Document and Preview the Document Uploaded options. Then, I added the Translation button that translates the user's document to the translation language set in Options. I have also added Save and Download options that help users to download the file that has been translated locally. Although I tried my best to finish the front-end from scratch as soon as possible, I couldn't yet finish fixing the back-end that is, Options page-that has depicts the translation settings. I feel fixing the back-end for the Desktop app will be the possible challenge for the new contributors as well.

I used a pre-built library "pdf.js" is a JavaScript library intended to render PDF files using the HTML5 Canvas for a safer and web standards compliant web browser rendering of PDF files. For all the text, word documents and editable files I coded a javascript 'File Uploaded Preview' function that renders the text from the document user uploaded and previews it in the provided text area. These features broaden the types of file documents to be uploaded upon users choice. The javascript Download function allows users to save the file which has been translated with their own naming conventions and download location.

Coding during the development of Desktop app was quite challenging as it involved all new technologies to be used. I had self-learnt libraries of ElectronJS and pdf.js which were interesting. 

## Screenshots of MindTheWord - Desktop Application

<table>
  <tr>
    <td><img src="https://i.imgur.com/VWufmAf.png"/></td>
    <td><img src="https://i.imgur.com/wsWhTzM.png"/></td>
    <td><img src="https://i.imgur.com/5g7aiCA.png" title="source: imgur.com" /></td>
  </tr>
  <tr>
    <td><img src="https://i.imgur.com/HHyAhfJ.png"/></td>
    <td><img src="https://i.imgur.com/zonDqQg.png"/></td>
    <td><img src="https://i.imgur.com/yBwcJbl.png"/></td>
  </tr>
</table>

<table>
	<tr>
		<td colspan="2"><img src="https://i.imgur.com/7KlY4wn.png" /></td>
	</tr>
</table>

## Things left to tackle

The **MindTheWord Desktop** app's back-end translation configurations have not yet been properly tackled. The back-end translations work to translate the uploaded document by the user will need some work and discussion with mentors and project admins. Development of front-end of the app is done.

### Thankyou!

The work was cool, learned all aspects of development. Although my project was of adding new features and development of new product, I still got to sufficiently interact with the code base which was good. Most importantly I gained exposure to this great great work that so many talented people involved in open source development do.

I would like to thank every AOSSIE member, especially my mentor, Bruno, for being so nice and helpful. I have learnt a lot in the past 3 months and it has been a great experience to be a part of this wonderful community. I am glad to take my learnings forward in my higher education and tech career. I also like to thank AOSSIE and Google for giving me this opportunity.

<img src="https://i.imgur.com/8qRBWC4.jpg" />

## Social

* My GitHub Gist can be found [here](https://gist.github.com/KukkadapuSushma/650212cb51b277c526c76d887c8f3913)

* My Twitter updates about AOSSIE and MindTheWord can be found [here](https://twitter.com/search?f=tweets&q=aossie%20from%3ASushmaKukkadapu&src=typd&lang=en-gb)

* My blog posts can be found [here](https://medium.com/@kukkadapusushma12/sushma-kukkadapu-google-summer-of-code-2018-aossie-e6134a055046) and [here](https://medium.com/@kukkadapusushma12/my-journey-into-google-summer-of-code-2018-1a1dc026e6bc)

